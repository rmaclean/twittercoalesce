﻿/*jslint browser: true, devel: true, windows: false, white: true */
/*globals jQuery: true*/
(function (twitterCoalesce, $, undefined) {
    "use strict";

    function getTwitterNames(ids) {
        var def = $.Deferred(),
            result = {
                success: false,
                names: [],
                errorMessage: ""
            };

        $.getJSON("https://api.twitter.com/1/users/lookup.json?user_id=" + ids.join(",") + " &callback=?", function (data) {
            result.success = true;
            $.each(data, function (index, item) {
                result.names.push({
                    name: item.name,
                    screen_name: item.screen_name,
                    toString: function () {
                        return this.screen_name;
                    }
                });
            });

            def.resolve(result);
        })
        .error(function (jqXHR) {
            result.success = false;
            switch (jqXHR.status) {
                case 400:
                    result.errorMessage = "Twitter rate limite hit.";
                    break;
            }

            def.resolve(result);
        });

        return def;
    }

    function buildNameResults(nameResult, currentList) {
        var result = {
            results: [],
            success: false,
            errorMessage: ""
        };

        if (nameResult.success) {
            result.success = true;
            result.results = currentList.concat(nameResult.names);
        } else {
            result.errorMessage = nameResult.errorMessage;
        }

        return result;
    }

    function getAllTwitterNames(ids) {
        var def = $.Deferred(),
            result = {
                results: [],
                success: true,
                errorMessage: ""
            },
            batch,
            more = true,
            counter,
            batchCounter = 0,
            batchStart = 0,
            batches = [],
            step = 100;

        while (more) {
            batch = [];
            for (counter = batchStart; counter < batchStart + step; counter++) {
                if (ids[counter] === undefined) {
                    more = false;
                    break;
                }

                batch.push(ids[counter]);
            }

            if (batch.length > 0) {
                batches.push(function (b) {
                    var x = $.Deferred();
                    getTwitterNames(b).then(function (nameResult) {
                        var buildResult = buildNameResults(nameResult, result.results);
                        if (buildResult.success) {
                            result.results = buildResult.results;
                        } else {
                            result.success = false;
                            result.errorMessage = buildResult.errorMessage;
                        }

                        x.resolve();
                    });

                    return x;
                }(batch));
            }

            batchCounter = batchCounter + 1;
            batchStart = batchCounter * step;
        }

        $.when.apply(this, batches).then(function () {
            def.resolve(result);
        });
           
        return def;
    }

    function getFollowerIdList(username) {
        var def = $.Deferred(),
            result = {
                errorMessage: "",
                success: false,
                ids: []
            };

        $.getJSON("https://api.twitter.com/1/followers/ids.json?cursor=-1&screen_name=" + username + "&callback=?", function (data) {
            result.success = true;
            result.ids = data.ids;
            def.resolve(result);
        })
        .error(function (jqXHR) {
            result.success = false;
            switch (jqXHR.status) {
                case 400:
                    result.errorMessage = "Twitter rate limite hit.";
                    break;
                default:
                    result.errorMessage = jqXHR.responseText;
                    break;
            }

            def.resolve(result);
        });

        return def;
    }

    twitterCoalesce.findFollowers = function (firstUsername, secondUsername) {
        var def = $.Deferred(),
            nameResult,
            result = {
                names: [],
                filtered: false,
                errorMessage: "",
                success: false
            };

        getFollowerIdList(firstUsername).then(function (firstFollowerResult) {
            if (!firstFollowerResult.success) {
                result.errorMessage = firstFollowerResult.errorMessage;
                def.resolve(result);
            } else {
                getFollowerIdList(secondUsername).then(function (secondFollowerResult) {
                    if (!secondFollowerResult.success) {
                        result.errorMessage = secondFollowerResult.errorMessage;
                        def.resolve(result);
                    } else {
                        if (firstFollowerResult.ids.length === 0 ||
                            secondFollowerResult.ids === 0) {
                            result.success = true;
                            def.resolve(result);
                        } else {
                            var ids = [];
                            $.each(firstFollowerResult.ids, function (index, item) {
                                if (secondFollowerResult.ids.indexOf(item) !== -1) {
                                    ids.push(item);
                                }
                            });

                            if (ids.length > 13000) {
                                result.filtered = true;
                                ids.splice(13000, ids.length - 13000);
                            }

                            getAllTwitterNames(ids).then(function (nameResult) {
                                if (!nameResult.success) {
                                    def.resolve(result);
                                } else {
                                    result.names = nameResult.results;
                                    result.success = true;
                                    def.resolve(result);
                                }
                            });
                        }
                    }
                });
            }
        });

        return def;
    };

    twitterCoalesce.makeListItems = function (results) {
        var result = "";
        $.each(results, function (index, item) {
            result += "<li><a href='http://twitter.com/" + item.screen_name + "' target='_blank'>" + item.name + "</a></li>";
        });

        return result;
    };

    twitterCoalesce.sortResults = function (results) {
        return results.sort(function (a, b) {
            if (a.name === b.name) {
                return 0;
            }

            if (a.name < b.name) {
                return -1;
            }

            return 1;
        });
    };

})(window.twitterCoalesce = window.twitterCoalesce || {}, jQuery);
